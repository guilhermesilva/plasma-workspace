# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-25 00:38+0000\n"
"PO-Revision-Date: 2022-03-17 18:06+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.08.1\n"

#. i18n: ectx: label, entry (cursorTheme), group (Mouse)
#: cursorthemesettings.kcfg:9
#, kde-format
msgid "Name of the current cursor theme"
msgstr "Namn på aktuellt markörtema"

#. i18n: ectx: label, entry (cursorSize), group (Mouse)
#: cursorthemesettings.kcfg:13
#, kde-format
msgid "Current cursor size"
msgstr "Aktuell markörstorlek"

#: kcmcursortheme.cpp:303
#, kde-format
msgid ""
"You have to restart the Plasma session for these changes to take effect."
msgstr "Plasma-sessionen måste startas om för att ändringarna ska verkställas."

#: kcmcursortheme.cpp:377
#, kde-format
msgid "Unable to create a temporary file."
msgstr "Kan inte skapa en tillfällig fil."

#: kcmcursortheme.cpp:388
#, kde-format
msgid "Unable to download the icon theme archive: %1"
msgstr "Kan inte ladda ner arkivet för ikontemat: %1"

#: kcmcursortheme.cpp:419
#, kde-format
msgid "The file is not a valid icon theme archive."
msgstr "Filen är inte ett giltigt arkiv för ikonteman."

#: kcmcursortheme.cpp:426
#, kde-format
msgid "Failed to create 'icons' folder."
msgstr "Misslyckades skapa katalogen 'icons'."

#: kcmcursortheme.cpp:435
#, kde-format
msgid ""
"A theme named %1 already exists in your icon theme folder. Do you want "
"replace it with this one?"
msgstr ""
"Ett tema med namn %1 finns redan i din katalog för ikonteman. Vill du "
"ersätta det med det här temat?"

#: kcmcursortheme.cpp:439
#, kde-format
msgid "Overwrite Theme?"
msgstr "Skriv över tema?"

#: kcmcursortheme.cpp:463
#, kde-format
msgid "Theme installed successfully."
msgstr "Tema installerat med lyckat resultat."

#. i18n: ectx: label, entry (cursorTimeout), group (BusyCursorSettings)
#. i18n: ectx: label, entry (taskbarTimeout), group (TaskbarButtonSettings)
#: launchfeedbacksettings.kcfg:19 launchfeedbacksettings.kcfg:33
#, kde-format
msgid "Timeout in seconds"
msgstr ""

#: plasma-apply-cursortheme.cpp:42
#, kde-format
msgid "The requested size '%1' is not available, using %2 instead."
msgstr "Den begärda storleken '%1' är inte tillgänglig, använder %2 istället."

#: plasma-apply-cursortheme.cpp:63
#, kde-format
msgid ""
"This tool allows you to set the mouse cursor theme for the current Plasma "
"session, without accidentally setting it to one that is either not "
"available, or which is already set."
msgstr ""
"Verktyget låder dig ställa in muspekartemat för den aktuella Plasma-"
"sessionen, utan att av misstag ställa in det till ett som antingen inte är "
"tillgängligt eller som redan används."

#: plasma-apply-cursortheme.cpp:67
#, kde-format
msgid ""
"The name of the cursor theme you wish to set for your current Plasma session "
"(passing a full path will only use the last part of the path)"
msgstr ""
"Namnet på pekartemat som du vill använda för den aktuella Plasma-sessionen "
"(om en fullständig sökväg anges används bara den senare delen av sökvägen)"

#: plasma-apply-cursortheme.cpp:68
#, kde-format
msgid ""
"Show all the themes available on the system (and which is the current theme)"
msgstr ""
"Visa alla tillgängliga teman på system (och vilket som är det aktuella temat)"

#: plasma-apply-cursortheme.cpp:69
#, kde-format
msgid "Use a specific size, rather than the theme default size"
msgstr "Använd en specifik storlek istället för temats standardstorlek"

#: plasma-apply-cursortheme.cpp:90
#, kde-format
msgid ""
"The requested theme \"%1\" is already set as the theme for the current "
"Plasma session."
msgstr ""
"Begärt tema \"%1\" är redan använt som tema för den aktuella Plasma-"
"sessionen."

#: plasma-apply-cursortheme.cpp:101
#, kde-format
msgid ""
"Successfully applied the mouse cursor theme %1 to your current Plasma session"
msgstr ""
"Införde muspekartemat %1 med lyckat resultat för den aktuella Plasma-"
"sessionen"

#: plasma-apply-cursortheme.cpp:103
#, kde-format
msgid ""
"You have to restart the Plasma session for your newly applied mouse cursor "
"theme to display correctly."
msgstr ""
"Du måste starta om Plasma-sessionen för att det nytillagda muspekartemat ska "
"visas korrekt."

#: plasma-apply-cursortheme.cpp:113
#, kde-format
msgid ""
"Could not find theme \"%1\". The theme should be one of the following "
"options: %2"
msgstr ""
"Kunde inte hitta temat \"%1\". Temat ska vara ett av följande alternativ: %2"

#: plasma-apply-cursortheme.cpp:121
#, kde-format
msgid "You have the following mouse cursor themes on your system:"
msgstr "Du har följande muspekarteman på systemet:"

#: plasma-apply-cursortheme.cpp:126
#, kde-format
msgid "(Current theme for this Plasma session)"
msgstr "(Aktuellt tema för Plasma-sessionen)"

#: ui/Delegate.qml:44
#, kde-format
msgid "Remove Theme"
msgstr "Ta bort tema"

#: ui/Delegate.qml:51
#, kde-format
msgid "Restore Cursor Theme"
msgstr "Återställ pekartema"

#: ui/LaunchFeedbackDialog.qml:16
#, kde-format
msgctxt "@title"
msgid "Launch Feedback"
msgstr ""

#: ui/LaunchFeedbackDialog.qml:25
#, kde-format
msgctxt "@info:usagetip"
msgid "Configure the animations played while an application is launching."
msgstr ""

#: ui/LaunchFeedbackDialog.qml:47
#, kde-format
msgctxt "@label"
msgid "Cursor feedback:"
msgstr ""

#: ui/LaunchFeedbackDialog.qml:48
#, kde-format
msgctxt "@option:radio No cursor feedback when launching apps"
msgid "None"
msgstr ""

#: ui/LaunchFeedbackDialog.qml:57
#, kde-format
msgctxt "@option:radio"
msgid "Static"
msgstr ""

#: ui/LaunchFeedbackDialog.qml:66
#, kde-format
msgctxt "@option:radio"
msgid "Blinking"
msgstr ""

#: ui/LaunchFeedbackDialog.qml:75
#, kde-format
msgctxt "@option:radio"
msgid "Bouncing"
msgstr ""

#: ui/LaunchFeedbackDialog.qml:90
#, kde-format
msgctxt "@label"
msgid "Task Manager feedback:"
msgstr ""

#: ui/LaunchFeedbackDialog.qml:92
#, kde-format
msgctxt "@option:check"
msgid "Enable animation"
msgstr ""

#: ui/LaunchFeedbackDialog.qml:111
#, kde-format
msgctxt "@label"
msgid "Stop animations after:"
msgstr ""

#: ui/LaunchFeedbackDialog.qml:125 ui/LaunchFeedbackDialog.qml:137
#, kde-format
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] ""
msgstr[1] ""

#: ui/main.qml:64 ui/main.qml:102
#, fuzzy, kde-format
#| msgid "Size:"
msgid "Size: %1"
msgstr "Storlek:"

#: ui/main.qml:117
#, kde-format
msgctxt "@action:button"
msgid "&Configure Launch Feedback…"
msgstr ""

#: ui/main.qml:131
#, kde-format
msgid "&Install from File…"
msgstr "&Installera från fil…"

#: ui/main.qml:137
#, fuzzy, kde-format
#| msgid "&Get New Cursors…"
msgid "&Get New…"
msgstr "&Hämta nya pekare…"

#: ui/main.qml:181
#, kde-format
msgid "Open Theme"
msgstr "Öppna tema"

#: ui/main.qml:183
#, kde-format
msgid "Cursor Theme Files (*.tar.gz *.tar.bz2)"
msgstr "Muspekartema-filer (*.tar.gz *.tar.bz2)"

#: xcursor/xcursortheme.cpp:58
#, kde-format
msgctxt ""
"@info The argument is the list of available sizes (in pixel). Example: "
"'Available sizes: 24' or 'Available sizes: 24, 36, 48'"
msgid "(Available sizes: %1)"
msgstr "(Tillgängliga storlekar: %1)"

#~ msgid "This module lets you choose the mouse cursor theme."
#~ msgstr "Den här modulen låter dig välja muspekartema."

#~ msgid "Name"
#~ msgstr "Namn"

#~ msgid "Description"
#~ msgstr "Beskrivning"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Stefan Asserhäll"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "stefan.asserhall@bredband.net"

#~ msgid "Cursors"
#~ msgstr "Pekare"

#~ msgid "(c) 2003-2007 Fredrik Höglund"
#~ msgstr "© 2003-2007 Fredrik Höglund"

#~ msgid "Fredrik Höglund"
#~ msgstr "Fredrik Höglund"

#~ msgid "Marco Martin"
#~ msgstr "Marco Martin"
